# Build Server Sample

This is a sample project to use with TeamCity to learn the basics of building a
project using Continuous Integration Techniques. Please follow the instructions
in the exercise section to get the project building.
